#pragma once 
#include <spring\Framework\IScene.h>

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include <boost\any.hpp>
#include <qstring.h>

namespace Spring
{
	class InitialScene : public IScene
	{
	public:
		explicit InitialScene(const std::string& ac_szSceneName);

		void createScene() override;
		void release() override;
		void createGUI();

	private:
		QWidget *centralWidget;
		QGridLayout *gridLayout_2;
		QGridLayout *gridLayout;
		QLabel *displayTimeLabel;
		QLabel *applicationNameLabel;
		QLabel *sampleRatelabel;
		QSpinBox *sampleRateSpinBox;
		QLabel *refreshRateLabel;
		QPushButton *okButton;
		QSpinBox *refreshRateSpinBox;
		QLineEdit *applicationNameLineEdit;
		QSpacerItem *verticalSpacer;
		QSpacerItem *verticalSpacer_2;
		QSpacerItem *horizontalSpacer_2;
		QSpacerItem *horizontalSpacer;
		QDoubleSpinBox *displayTimeDoubleSpinBox;
		QMenuBar *menuBar;
		QToolBar *mainToolBar;
		QStatusBar *statusBar;
	};
}
