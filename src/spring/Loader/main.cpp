#include <spring\Framework\Application.h>
#include <spring\Application\ApplicationModel.h>
#include <qapplication.h>

int main(int argc, char **argv)
{
	QApplication qApplication(argc, argv);

	Spring::Application& application = Spring::Application::getInstance();

	Spring::ApplicationModel applicationModel;

	application.setApplicationModel(&applicationModel);

	application.start("Siemens", 420, 420);

	qApplication.exec();

	return 0;
}