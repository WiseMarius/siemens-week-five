#include "..\..\..\include\spring\Application\InitialScene.h"

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName):IScene(ac_szSceneName)
	{
	}
	void InitialScene::createScene()
	{
		createGUI();
	}
	void InitialScene::release()
	{
		delete centralWidget;
	}
	void InitialScene::createGUI()
	{
		m_uMainWindow.get()->resize(801, 537);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout_2 = new QGridLayout(centralWidget);
		gridLayout_2->setSpacing(6);
		gridLayout_2->setContentsMargins(11, 11, 11, 11);
		gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		displayTimeLabel = new QLabel(centralWidget);
		displayTimeLabel->setObjectName(QStringLiteral("displayTimeLabel"));

		gridLayout->addWidget(displayTimeLabel, 3, 1, 1, 1);

		applicationNameLabel = new QLabel(centralWidget);
		applicationNameLabel->setObjectName(QStringLiteral("applicationNameLabel"));

		gridLayout->addWidget(applicationNameLabel, 1, 1, 1, 1);

		sampleRatelabel = new QLabel(centralWidget);
		sampleRatelabel->setObjectName(QStringLiteral("sampleRatelabel"));

		gridLayout->addWidget(sampleRatelabel, 2, 1, 1, 1);

		sampleRateSpinBox = new QSpinBox(centralWidget);
		sampleRateSpinBox->setObjectName(QStringLiteral("sampleRateSpinBox"));

		gridLayout->addWidget(sampleRateSpinBox, 2, 3, 1, 1);

		refreshRateLabel = new QLabel(centralWidget);
		refreshRateLabel->setObjectName(QStringLiteral("refreshRateLabel"));

		gridLayout->addWidget(refreshRateLabel, 4, 1, 1, 1);

		okButton = new QPushButton(centralWidget);
		okButton->setObjectName(QStringLiteral("okButton"));

		gridLayout->addWidget(okButton, 5, 5, 1, 1);

		refreshRateSpinBox = new QSpinBox(centralWidget);
		refreshRateSpinBox->setObjectName(QStringLiteral("refreshRateSpinBox"));

		gridLayout->addWidget(refreshRateSpinBox, 4, 3, 1, 1);

		applicationNameLineEdit = new QLineEdit(centralWidget);
		applicationNameLineEdit->setObjectName(QStringLiteral("applicationNameLineEdit"));

		gridLayout->addWidget(applicationNameLineEdit, 1, 3, 1, 1);

		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer, 0, 1, 1, 1);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer_2, 5, 2, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer_2, 2, 5, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 2, 0, 1, 1);

		displayTimeDoubleSpinBox = new QDoubleSpinBox(centralWidget);
		displayTimeDoubleSpinBox->setObjectName(QStringLiteral("displayTimeDoubleSpinBox"));

		gridLayout->addWidget(displayTimeDoubleSpinBox, 3, 3, 1, 1);


		gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

		m_uMainWindow.get()->setCentralWidget(centralWidget);
		menuBar = new QMenuBar(m_uMainWindow.get());
		menuBar->setObjectName(QStringLiteral("menuBar"));
		menuBar->setGeometry(QRect(0, 0, 801, 26));
		m_uMainWindow.get()->setMenuBar(menuBar);
		mainToolBar = new QToolBar(m_uMainWindow.get());
		mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
		m_uMainWindow.get()->addToolBar(Qt::TopToolBarArea, mainToolBar);
		statusBar = new QStatusBar(m_uMainWindow.get());
		statusBar->setObjectName(QStringLiteral("statusBar"));
		m_uMainWindow.get()->setStatusBar(statusBar);

		displayTimeLabel->setText(QApplication::translate("MainWindow", "Display Time", Q_NULLPTR));
		applicationNameLabel->setText(QApplication::translate("MainWindow", "Application Name", Q_NULLPTR));
		sampleRatelabel->setText(QApplication::translate("MainWindow", "Sample Rate", Q_NULLPTR));
		refreshRateLabel->setText(QApplication::translate("MainWindow", "Refresh Rate", Q_NULLPTR));
		okButton->setText(QApplication::translate("MainWindow", "OK", Q_NULLPTR));

		std::string defaultAppName = boost::any_cast<std::string>(m_TransientDataCollection.find("ApplicationNameKey")->second);
		applicationNameLineEdit->setText(defaultAppName.c_str());


	}
}
