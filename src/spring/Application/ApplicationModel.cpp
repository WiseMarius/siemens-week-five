#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>

const std::string initialSceneName = "InitialScene";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{

	}

	void ApplicationModel::defineScene()
	{
		IScene* initialScene = new InitialScene(initialSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		std::string appName = "Undefined";
		m_TransientData.emplace("ApplicationNameKey", appName);
		m_TransientData.emplace("SampleRateKey", 25600);
		m_TransientData.emplace("DisplayTimeKey", 0.1);
		m_TransientData.emplace("RefreshRateKey", 1);
	}
}
